sap.ui.define([ 'sap/ui/core/mvc/Controller', 'sap/m/MessageToast' ], function(
		oController, oMessageToast) {
	return oController.extend("com.ui.sample.controller.Master", {
		onInit : function() {

		}, // End of onInit

		onAfterRendering : function() {
			this.getView().getModel().read("/ProjScenDetails", {
				success : function(d, r) {
					debugger;
				},
				error : function(er) {
					debugger;
				}
			});
			var oTree = this.getView().byId("idTreeTable");
			oTree.bindRows({
				path : '/ProjScenDetails',
				properties : {
					navigation : {
						'ProjScenDetails' : 'ProjScenDetails'
					}
				}
			});
		},

		onNavBack : function() {
			window.history.go(-1);
		},

		// --- Navigation
		onBtnPress : function(oEvent) {
		}
	});
});