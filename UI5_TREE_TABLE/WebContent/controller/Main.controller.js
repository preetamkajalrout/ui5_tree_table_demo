sap.ui.define([ 'sap/ui/core/mvc/Controller',
                'sap/m/MessageToast' ], 
		function(oController, oMessageToast) {
			return oController.extend("com.ui.sample.controller.Main", {
				onInit: function() {
					if (sap.ui.Device.support.touch === false) {
						this.getView().addStyleClass("sapUiSizeCompact");
					}
				} //End of onInit
			});
});